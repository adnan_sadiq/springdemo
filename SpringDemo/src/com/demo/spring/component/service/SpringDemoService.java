package com.demo.spring.component.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.spring.jpa.entities.MyInfo;
import com.demo.spring.jpa.entities.repository.MyInfoMetadataRepository;
import com.demo.spring.jpa.entities.repository.MyInfoRepository;

@Service
public class SpringDemoService {

	@Autowired
	MyInfoMetadataRepository metaDAO;

	@Autowired
	MyInfoRepository infoDAO;

	public void postSomething(String name, String comments) {

		// STEP 01: READ SOMETHING
		List<String> listAddress = infoDAO.customQueryToGetAddress("na");
		
		listAddress.stream().forEach(System.out::println);

		// STEP 02: SAVE NEW RECORD
		MyInfo myInfo = new MyInfo();
		myInfo.setName(name);
		myInfo.setOthers(comments);
		infoDAO.save(myInfo);
	}
}
