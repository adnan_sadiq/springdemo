package com.demo.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.demo.spring.component.service.SpringDemoService;
import com.demo.spring.component.service.model.SpringDemoServiceModel;

@Controller
public class SpringDemoController {

	@Autowired
	SpringDemoService service;

	@RequestMapping(value = "welcome", method = RequestMethod.GET)
	public ModelAndView get6ee() {

		ModelAndView model = new ModelAndView("welcome");

		model.addObject("title", "Title from spring controller");
		model.addObject("message", "Message from spring controller");

		return model;
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String register(@ModelAttribute("inputDTO") SpringDemoServiceModel inputDTO) {

		service.postSomething(inputDTO.getName(), inputDTO.getDetails());

		return "redirect:/welcome";

	}
}
