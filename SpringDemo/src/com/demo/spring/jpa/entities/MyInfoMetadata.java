package com.demo.spring.jpa.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.stereotype.Repository;

/**
 * The persistent class for the MyInfoMetadata database table.
 * 
 */

@Entity
@Table(name = "MyInfoMetadata", schema = "dbo", catalog = "SpringDemo")
@NamedQuery(name = "MyInfoMetadata.findAll", query = "SELECT m FROM MyInfoMetadata m")
public class MyInfoMetadata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MYINFOMETA_ID_GENERATOR", sequenceName = "SPRING_DEMO_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MYINFOMETA_ID_GENERATOR")
	@Column(name = "ID")
	private long id;

	@Column(name = "Age")
	private int age;

	@Column(name = "Others")
	private String others;

	// bi-directional many-to-one association to MyInfo
	@ManyToOne
	@JoinColumn(name = "MyInfoID")
	private MyInfo myInfo;

	public MyInfoMetadata() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getOthers() {
		return this.others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public MyInfo getMyInfo() {
		return this.myInfo;
	}

	public void setMyInfo(MyInfo myInfo) {
		this.myInfo = myInfo;
	}

}