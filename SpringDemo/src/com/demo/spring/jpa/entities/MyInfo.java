package com.demo.spring.jpa.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The persistent class for the MyInfo database table.
 * 
 */
@Entity
@NamedQuery(name = "MyInfo.findAll", query = "SELECT m FROM MyInfo m")
@Table(name = "MyInfo", schema = "dbo", catalog = "SpringDemo")
public class MyInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MYINFO_ID_GENERATOR", sequenceName = "SPRING_DEMO_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MYINFO_ID_GENERATOR")
	@Column(name = "ID")
	private long id;

	@Column(name = "Address")
	private String address;

	@Column(name = "Name")
	private String name;

	@Column(name = "Others")
	private String others;

	// bi-directional many-to-one association to MyInfoMetadata
	@OneToMany(mappedBy = "myInfo")
	private List<MyInfoMetadata> myInfoMetadata;

	public MyInfo() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOthers() {
		return this.others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public List<MyInfoMetadata> getMyInfoMetadata() {
		return this.myInfoMetadata;
	}

	public void setMyInfoMetadata(List<MyInfoMetadata> myInfoMetadata) {
		this.myInfoMetadata = myInfoMetadata;
	}

	public MyInfoMetadata addMyInfoMetadata(MyInfoMetadata myInfoMetadata) {
		getMyInfoMetadata().add(myInfoMetadata);
		myInfoMetadata.setMyInfo(this);

		return myInfoMetadata;
	}

	public MyInfoMetadata removeMyInfoMetadata(MyInfoMetadata myInfoMetadata) {
		getMyInfoMetadata().remove(myInfoMetadata);
		myInfoMetadata.setMyInfo(null);

		return myInfoMetadata;
	}

}