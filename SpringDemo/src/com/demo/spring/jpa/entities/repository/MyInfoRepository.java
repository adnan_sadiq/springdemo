package com.demo.spring.jpa.entities.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.demo.spring.jpa.entities.MyInfo;

@Repository
public interface MyInfoRepository extends JpaRepository<MyInfo, Long> {

	List<MyInfo> findByName(String name);

	@Query("select i.address from MyInfo i where i.address=:address")
	List<String> customQueryToGetAddress(@Param("address") String address);
	


}
