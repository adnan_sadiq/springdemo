package com.demo.spring.jpa.entities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.spring.jpa.entities.MyInfoMetadata;
@Repository
public interface MyInfoMetadataRepository extends JpaRepository<MyInfoMetadata, Long> {

}
